import BuildModel, {BuildFlags} from "../models/BuildModel";
import React from "react";
import PropTypes from "prop-types";

const BuildWarning = (props) => {
    if (props.build.hasFlag(BuildFlags.UPGRADED_BUILD)) {
        return <div className="notification is-warning is-light">
            {"Ce build a été réalisé pour une version antérieure de Dauntless et peut être obsolète."}
        </div>;
    }

    if (props.build.hasFlag(BuildFlags.INVALID_BUILD)) {
        return <div className="notification is-danger is-light">
            {"Ce build est cassé, plusieurs pièces ont été automatiquement déséquipées car elles ne sont plus" +
            "compatible avec la version actuelle de Dauntless."}
        </div>;
    }

    return null;
};

BuildWarning.propTypes = {
    build: PropTypes.objectOf(BuildModel),
};

export default BuildWarning;
